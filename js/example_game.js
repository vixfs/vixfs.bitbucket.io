let level,
    // dataLevels - исходные данные уровней
    dataLevels = [
    {
        minPoints: 100,
        maxPoints: 500,
        time: 10, // в секундах
        delay_time:2, //задержка перед началом игры, не обязательно
    },
    {
        minPoints: 300,
        maxPoints: 600,
        time: 15, // в секундах
    }
];

function newGame() {
    // Удалить. Кнопки Начать игру, Правильный ответ...
    document.querySelector(".start").style.display = "none";
    document.querySelector(".buttons_field").style.display = "block";
    document.querySelector('.button_correct_answer').disabled = false;
    document.querySelector('.button_wrong_answer').disabled = false;
    document.querySelector('.button_end_session').disabled = false;
    //

    initProgressLine();

    /** При правильном/неправильном ответе изменение количества очков
     * @param {number} points - кол-во очков за ответ (положительное или отрицательное число)
     *
     * changePoints(points);
     */


    /** Закончить сессию
     *
     * changeLevelState();
     */


}


//Получение уровня и передача данных уровня (min,max очки, время) в шапку
function getDataLevel(startLevel) {
    let dataLevel = dataLevels[startLevel-1];
    level = startLevel;

    return dataLevel;
}